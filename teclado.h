#ifndef TECLADO_H_
#define TECLADO_H_

#include "stm32f10x.h"

uint8_t KEYPAD_Update (uint8_t *pkey);
void tecladoInit(void);

#endif /* TECLADO_H_ */